﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace timelapseCliTool
{
    public class TimeLapseTool 
    {
        Params p;

        public TimeLapseTool(Params p)
        {
            // globalInit();
            this.p = p;
        }
       public TimeLapseTool()
        {
            p = new Params();
        }
        

        public List<string> filesList { get { return new List<string>(p.images); } }


       
       /* public string makeTimeLapse()
        {
            return makeTimeLapse(Accord.Video.FFMPEG.VideoCodec.Raw);
        }*/
        public string makeTimeLapse()
        {
            var size = new Size(p.width,
                        p.height);
            // The desired size of the video
            //fps = (int)(1.0F / ((float)numericUpDown_ms.Value / 1000.0F));                                       // The desired frames-per-second
           
            if (System.IO.File.Exists(p.outFile)) System.IO.File.Delete(p.outFile);
            // var destinationfile = @".\out\myfile.avi";             // Output file
            /*  var srcdirectory = @"d:\foo\bar";                   // Directory to scan for images
              var pattern = "*.jpg";                              // Files to look for in srcdirectory
              var searchoption = SearchOption.TopDirectoryOnly;   // Search Top Directory Only or all subdirectories (recursively)?*/
			var writer = new Accord.Video.FFMPEG.VideoFileWriter ();
			using ( writer = new Accord.Video.FFMPEG.VideoFileWriter())          // Initialize a VideoFileWriter
            {
                writer.Open(p.outFile,
                            size.Width,
                           size.Height,
                           p.fps,
                           p.codec, p.bitrate);              // Start output of video

                foreach (var file  in p.images.Select((value, i) => new { i, value }))//Directory.GetFiles(srcdirectory, pattern, searchoption))   // Iterate files
                {
                    if (file.value == "") break;
                    using (var original = (Bitmap)Image.FromFile(file.value))
                    {   // Read bitmap
                        //  original.SetResolution(300, 300);
                        // int i = 0;
                        using (var resized = new Bitmap(original, size))
                        {        // Resize if necessary
                                 //resized.Save(@".\out\file" + i + ".jpg");
                                 //  resized.SetResolution(300, 300);
                            try { Console.Clear(); } catch { }
                            //showAsciiScreen();
                            float pc = (100.0F / p.images.Count) * file.i;
                            Console.Write((int)pc);
                            Console.Write("%\t");
                            Console.Write(file.i);
                            Console.Write("/");
                            Console.Write(p.images.Count);
                            Console.WriteLine("\t");
                            writer.WriteVideoFrame(resized);                    // Write frame
                            //if (pgv != null) { pgv.Value = file.i ;
                            //    Thread.Sleep(200);
                            //}
                            
                        }
                    }
                }
                writer.Close();
                return p.outFile;
            }
        }
    }
   public  class Params
    {
         int _fps;
         int _bitrate;
         String _outFile;
         List<String> _images;
         Accord.Video.FFMPEG.VideoCodec _codec;
         int _height, _width;

         int _imageWmin=0, _imageWmax = 0, _imageHmin = 0, _imageHmax = 0;
         Boolean _checkEntries;

        public Params()
        {
            _images = new List<string>();
            _fps = 24;
            height = 768;
            _width = 1366;
            _bitrate = 5 * 1024 * 1024;
            _outFile = @"./out.avi";
            _codec = Accord.Video.FFMPEG.VideoCodec.MPEG4;
        }
        public Params(string[] args)
        {
            _images = new List<string>();
            _fps = 24;
            _height = 768;
            _width = 1366;
            _bitrate = 5 * 1024 * 1024;
            _outFile = @"./out.avi";
            _codec = Accord.Video.FFMPEG.VideoCodec.MPEG4;
            for (int i = 0; i < args.Length; i++)
            {
                int tmp = i;
                List<String> lst = valuesParam(out tmp, i, args);
                switch (args[i])
                {
                    case "-f": _fps = int.Parse(lst[0]); break;
                    case "-k": _bitrate = int.Parse(lst[0]); break;
                    case "-c":

                        switch (lst[0])
                        {
                            case "MPEG4":
                                _codec = Accord.Video.FFMPEG.VideoCodec.MPEG4; break;
                            case "H264":
                                _codec = Accord.Video.FFMPEG.VideoCodec.H264; break;
                            case "RAW":
                                _codec = Accord.Video.FFMPEG.VideoCodec.Raw; break;
                            case "FFVHUFF":
                                _codec = Accord.Video.FFMPEG.VideoCodec.FFVHUFF; break;
                        }

                                break;
                    case "-o": _outFile = lst[0]; break;
                    case "-i": _images = lst; break;
                    default: break;
                }
                i =tmp;
                //-f 27 -w 1366 -h 768 -o d:\out.avi -k 5242880 -c MPEG4 -i E:\photos\102EOS5D\H97C0004.JPG E:\photos\102EOS5D\H97C0006.JPG E:\photos\102EOS5D\H97C0009.JPG E:\photos\102EOS5D\H97C0011.JPG E:\photos\102EOS5D\H97C0012.JPG E:\photos\102EOS5D\H97C0015.JPG E:\photos\102EOS5D\H97C0016.JPG E:\photos\102EOS5D\H97C0018.JPG E:\photos\102EOS5D\H97C0021.JPG E:\photos\102EOS5D\H97C9950.JPG E:\photos\102EOS5D\H97C9951.JPG E:\photos\102EOS5D\H97C9952.JPG E:\photos\102EOS5D\H97C9953.JPG E:\photos\102EOS5D\H97C9954.JPG E:\photos\102EOS5D\H97C9955.JPG E:\photos\102EOS5D\H97C9956.JPG E:\photos\102EOS5D\H97C9957.JPG E:\photos\102EOS5D\H97C9963.JPG E:\photos\102EOS5D\H97C9964.JPG E:\photos\102EOS5D\H97C9965.JPG E:\photos\102EOS5D\H97C9966.JPG E:\photos\102EOS5D\H97C9967.JPG E:\photos\102EOS5D\H97C9968.JPG E:\photos\102EOS5D\H97C9969.JPG E:\photos\102EOS5D\H97C9970.JPG E:\photos\102EOS5D\H97C9971.JPG E:\photos\102EOS5D\H97C9972.JPG E:\photos\102EOS5D\H97C9973.JPG E:\photos\102EOS5D\H97C9974.JPG E:\photos\102EOS5D\H97C9975.JPG E:\photos\102EOS5D\H97C9976.JPG E:\photos\102EOS5D\H97C9977.JPG E:\photos\102EOS5D\H97C9978.JPG E:\photos\102EOS5D\H97C9979.JPG E:\photos\102EOS5D\H97C9980.JPG E:\photos\102EOS5D\H97C9981.JPG E:\photos\102EOS5D\H97C9982.JPG E:\photos\102EOS5D\H97C9983.JPG E:\photos\102EOS5D\H97C9984.JPG E:\photos\102EOS5D\H97C9985.JPG E:\photos\102EOS5D\H97C9986.JPG E:\photos\102EOS5D\H97C9987.JPG E:\photos\102EOS5D\H97C9988.JPG E:\photos\102EOS5D\H97C9989.JPG E:\photos\102EOS5D\H97C9990.JPG E:\photos\102EOS5D\H97C9991.JPG E:\photos\102EOS5D\H97C9992.JPG E:\photos\102EOS5D\H97C9993.JPG E:\photos\102EOS5D\H97C9994.JPG E:\photos\102EOS5D\H97C9995.JPG E:\photos\102EOS5D\H97C9996.JPG E:\photos\102EOS5D\H97C9997.JPG E:\photos\102EOS5D\H97C9998.JPG E:\photos\102EOS5D\H97C9999.JPG
            }
        }
        
        private List<String> valuesParam(out int stop, int start, string[] args)
        {
            List<String> r = new List<string>();
            stop = start+1 ;
            do
            {
                r.Add(args[stop]);
                stop++;
            }
            while ( stop<args.Length &&!args[stop].StartsWith("-"));

            stop--;
            return r;

        }
        public int loadImage(string dir)
        {
            if (this._images==null) this._images = new List<string>();

            _images.Add(dir); return _images.Count;
        }
        public int loadImageDir(string dir)
        {
            int r = 0;
            string[] files = System.IO.Directory.GetFiles(dir);
            this._images = new List<string>();
            foreach (string file in files)
            {

                if (file.ToLower().EndsWith(".jpg") || file.ToLower().EndsWith(".png") || file.EndsWith(".bmp") || file.EndsWith(".tif"))
                { _images.Add(file); r++; }
                System.Console.WriteLine(file);
            }

            return _images.Count;
        }
        public string makeParamString()
        {
            StringBuilder strb = new StringBuilder();
            strb.Append("-f ");
            strb.Append(fps);
            strb.Append(" -w ");
            strb.Append(width);
            strb.Append(" -h ");
            strb.Append(height);
            strb.Append(" -o ");
            strb.Append(outFile);
            strb.Append(" -k ");
            strb.Append(bitrate);
            strb.Append(" -c ");
            strb.Append(codec.ToString());
            strb.Append(" -i ");
            foreach(var s   in images.Select((value, index) => new { value, index }))
            {
                strb.Append(s.value);
                if (s.index != _images.Count + 1) { strb.Append(" "); }
            }
            return strb.ToString();

        }

        public void clearList()
        {
            _images=new List<string>();
        }

        internal Params clone()
        {
             return new timelapseCliTool.Params(this.makeParamString().Split(' '));
        }

        public int fps { get { return _fps; } set { _fps = value; } }
         public int bitrate { get { return _bitrate; } set { _bitrate = value; } }
      
        public int width { get { return _width; } set { _width = value; } }
        public int height { get { return _height; } set { _height = value; } }
        public string outFile { get { return _outFile; } set { _outFile = value; } }
        public int globalTime { get { return ((int)(1.0F / _fps * _images.Count)); } set { _fps = (int)(1.0F / (value / (float)_images.Count)); } }
        public int fpsByImageDuration { get { return (int)(1.0F / _fps); } set { _fps = (int)(1.0F / value); } }
        public Accord.Video.FFMPEG.VideoCodec codec { get { return _codec; }set { _codec = value; } }
        public List<string> images { get { return _images; }   }
    }
}
