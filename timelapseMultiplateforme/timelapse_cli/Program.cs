﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using timelapseCliTool;

namespace timelapse_cli
{
    class Program
    {
        static void Main(string[] args)
        {
            Params p = new Params(args);
            TimeLapseTool tst = new TimeLapseTool(p);
           // int t = 0;
            tst.makeTimeLapse();
            Console.WriteLine("end:" + p.outFile);
        }
    }
}
