﻿using System;
using Gtk;


public partial class MainWindow: Gtk.Window
{
	Gtk.ListStore musicListStore;
	timelapseCliTool.Params p=new timelapseCliTool.Params();
    private bool lowMemWindow = false;

    public MainWindow () : base (Gtk.WindowType.Toplevel)
	{
		Build ();
      //  this.lowMemWindow = true;
        myInit();
	}
    public MainWindow(Boolean lowMemWindow) : base(Gtk.WindowType.Toplevel)
    {
        Build();
        this.lowMemWindow = lowMemWindow;
        myInit();
        
    }
    protected void myInit()
    {
        treeview1.AppendColumn("Icon", new Gtk.CellRendererPixbuf(), "pixbuf", 0);
        treeview1.AppendColumn("file", new Gtk.CellRendererText(), "text", 1);
        if(!lowMemWindow)
            musicListStore = new Gtk.ListStore(typeof(Gdk.Pixbuf), typeof(string));
        else
            musicListStore = new Gtk.ListStore( typeof(string));
        treeview1.Model = musicListStore;
    }
    protected void OnDeleteEvent (object sender, DeleteEventArgs a)
	{
		Application.Quit ();
		a.RetVal = true;
	}
	protected void QuitSignalManager (object sender, EventArgs e)
	{

		Application.Quit ();
		 
	}

	public void fillTableImage(){

        int i = 0;
        foreach (var item  in p.images ) {
            addInList(item.ToString());
//musicListStore.AppendValues((new Gdk.Pixbuf (item)).ScaleSimple(64, 64, Gdk.InterpType.Bilinear), item.ToString());
            i++;
          }
		treeview1.ShowAll ();
		///	treeview1.Model.
	}
    protected void addInList(string v) {
        System.IO.Stream str = null;
        try
        {
            str = new System.IO.FileStream(v, System.IO.FileMode.Open);

            if (!lowMemWindow)
            {

                musicListStore.AppendValues((new Gdk.Pixbuf(str)).ScaleSimple(16, 16, Gdk.InterpType.Bilinear), v);

            }else { musicListStore.AppendValues(v); }
        }
        catch (Exception e)
        {
            if (!lowMemWindow)
            {
                musicListStore.AppendValues((new Gdk.Pixbuf(@"..\..\ERRORBLK.png")).ScaleSimple(64, 64, Gdk.InterpType.Bilinear), v.ToString());
            }
            else { musicListStore.AppendValues(v); }
        }
       finally { if(str!=null)str.Close(); }


    }
	protected void img_s_ValueChanged (object sender, EventArgs e)
	{

		int img_dur =(int)((1.0F/(float) hscale1.Value)*1000.0F);
		labelImg_duration.Text = img_dur.ToString ();
	//	throw new NotImplementedException ();
	}
	protected void imageListAdd (object sender, EventArgs e)
	{  
		Gtk.FileChooserDialog fc=
			new Gtk.FileChooserDialog("Choose the file to open",
				this, 
				Gtk.FileChooserAction.SelectFolder,
				"Cancel",Gtk.ResponseType.Cancel,
				"Open",Gtk.ResponseType.Accept);
		fc.Filter = new FileFilter ();
	/*	fc.Filter.AddPattern ("*.png");
		fc.Filter.AddPattern ("*.jpg");
		fc.Filter.AddPattern ("*.jpeg");
		//fc.SelectMultiple = true;*/

		if (fc.Run() == (int)Gtk.ResponseType.Accept) 
		{
		//	b_Next.Sensitive = true;
			p.loadImageDir(fc.Filename); 
			this.labelImg_count.Text = p.images.Count.ToString () ;
			fillTableImage ();

		}
		//Destroy() to close the File Dialog
		fc.Destroy();
			
	//	throw new NotImplementedException ();
	}

	 
	protected void imageListeDestroy (object sender, EventArgs e)
	{



		MessageDialog md = new MessageDialog (this,DialogFlags.Modal,MessageType.Question,ButtonsType.YesNo, "Are You Sure?\nVoullez vous vraiment supprimer la liste ?");
		md.Title = "Etes vous sur ?";
		ResponseType tp = (Gtk.ResponseType)md.Run();   
		md.Destroy(); 
		if(tp==Gtk.ResponseType.Yes)		 
		p.clearList ();
		this.labelImg_count.Text = p.images.Count.ToString () ;
		return;

		//throw new NotImplementedException ();
	}

	protected void exeTimelapse (object sender, EventArgs e)
	{
		var x = new timelapseCliTool.TimeLapseTool (p);
		x.makeTimeLapse ();
	//	throw new NotImplementedException ();
	}
	protected void destroyList (object sender, EventArgs e)
	{
        

        throw new NotImplementedException ();
	}


 

	 
	protected void Quitter (object sender, EventArgs e)
	{
		this.Destroy ();
		Application.Quit ();
	}


	 
	protected void ViewImageTouched (object sender, EventArgs e)
	{
		TreeView x=(( TreeView) sender);TreePath tp=x.Selection.GetSelectedRows()[0];


		var model = x.Model;
		TreeIter iter;
		model.GetIter (out iter, tp);
        string value = "";
        //	Console.WriteLine (value);
        try
        {
            value = model.GetValue(iter, 1).ToString();
            image5.Pixbuf = (new Gdk.Pixbuf(value)).ScaleSimple(image5.Allocation.Width, image5.Allocation.Height,
            Gdk.InterpType.Bilinear);
        }catch(Exception ex)
        {
            Console.WriteLine(ex.Message);
            var v = model.GetValue(iter, 0);

         if(!lowMemWindow)   image5.Pixbuf =((Gdk.Pixbuf) v).ScaleSimple(image5.Allocation.Width, image5.Allocation.Height,
            Gdk.InterpType.Bilinear);
        }
		//throw new NotImplementedException ();
	}
}