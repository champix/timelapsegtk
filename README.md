# README #
# Timelapse tool #

# State of project : *** ** pre alpha ** *** #
### Screenshot ###


### What is the mission of timelapse tool v0.4 ###
* Assemble photo folder in a AVI video file 
* Make easy selection of picture selected for video
* Multi OS usable (WIN : XP, 7, 8, 10 ; Mac OSX; linux; and all other compatible GTK and Mono.net)

### What is new in version 0.4 ###

* Support OSX, and linux
* list picture is shown
* Based on v0.3 objects adapted from Ms.net to Mono.NET
* default config saving 
* remodeled windows in adaptation of Ms.NET version

### What is deprecied in v0.4 ###

* VLC viewer for avi generated
* some no multi-OS Dependencies